# Tribe Wearables Demo Application

A barebones app that demonstrates the usage of the *Tribe BLE Library*. It comprises of a single Activity that consumes the library.

### The latest signed release can be found in the repository's [downloads](https://bitbucket.org/tribe/tribe-wearables-demo/downloads/).

More information on how to use can be found in the [wiki](https://bitbucket.org/tribe/tribe-wearables-demo/wiki/Home).