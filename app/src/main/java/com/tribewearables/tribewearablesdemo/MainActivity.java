package com.tribewearables.tribewearablesdemo;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.tribewearables.tribeble.TribeBleService;

import org.json.JSONArray;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Enables the Android device's Bluetooth radio and binds to the Tribe BLE Service.
     */
    private void startBluetooth() {
        // Find Bluetooth service and adapter.
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensure Bluetooth is enabled on the device.
        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLE);
        } else {
            // Start the BLE service.
            log("Starting BLE service...");
            Intent getServiceIntent = new Intent(this, TribeBleService.class);
            bindService(getServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        }
    }

    /**
     * A standard Android ServiceConnection. For the purposes of this demo a scan command is issued
     * as soon as the service is connected, unless there is already a connection in-place and the
     * Activity was just resumed.
     */
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // Get the service instance.
            mService = ((TribeBleService.LocalBinder) service).getService();
            log("Connected to the service.");

            // Initialize the service.
            mService.init();
            mServiceConnected = true;

            // Reconnect, if the Activity is resumed or initiate a scan.
            if (mDeviceConnected) {
                connect();
            } else {
                scan();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            log("Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }
    };

    //region Command Issuing

    /**
     * Starts scanning for BLE devices through the service. Results are received as
     * broadcasts from the service.
     */
    private void scan() {
        if (mServiceConnected) {
            mService.scan();
            log("Scanning...");
        }
    }

    /**
     * Establishes a connection to the selected Tribe Wearables device.
     */
    private void connect() {
        if (mServiceConnected) {
            mService.connect();
            log("Connecting...");
        }
    }

    /**
     * Initiates a workout.
     */
    private void startWorkout() {
        if (!mServiceConnected || mService == null) return;
        mService.startWorkout();

        mTextRepetitions.setText("0");
        mInWorkout = true;
        mStartButton.setEnabled(false);
        mStopButton.setEnabled(true);
        mFormatButton.setEnabled(false);
        mSendButton.setEnabled(false);
        mUploadButton.setEnabled(false);
    }

    /**
     * Terminates the current workout.
     */
    private void stopWorkout() {
        if (!mServiceConnected || mService == null) return;
        mService.stopWorkout();
        mService.listWorkouts();

        mInWorkout = false;
        mStartButton.setEnabled(true);
        mStopButton.setEnabled(false);
        mSendButton.setEnabled(true);
        mUploadButton.setEnabled(true);
        mFormatButton.setEnabled(true);
    }

    /**
     * Request the list of all workouts saved in the Tribe Wearables device's built-in flash.
     */
    private void listWorkouts() {
        if (mServiceConnected) {
            mSendButton.setEnabled(false);
            mUploadButton.setEnabled(false);
            mService.listWorkouts();
        }
    }

    /**
     * Select the Tribe Wearables device with which to interact with.
     * @param deviceAddress
     */
    private void select(String deviceAddress) {
        mService.select(deviceAddress);
    }

    private void formatMemory() { mService.formatMemory(); }

    //endregion Command Issuing

    //region Service Communication (Reception)

    /**
     * The service emits communicates with its clients through broadcasts. The following receiver is
     * an example of how to react to those messages. More information can be found in the wiki.
     */
    private final BroadcastReceiver mBleUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action == null) return;
            switch (action) {

                case TribeBleService.ACTION_BLESCAN_RESULT:
                    // A new device has been found. Its MAC address and name are included in the
                    // Intent passed with the broadcast. For the purposes of this demo the first
                    // device found is also the selected device.

                    // Extract the name from the Intent and check if it is a Tribe Wearables device
                    // before selecting it.
                    String name = intent.getStringExtra("name");
                    if (name != null && name.contains("WISH")) {
                        mDeviceAddress = intent.getStringExtra("address");
                        log("Found: " + name + " [" + mDeviceAddress + "]");
                        select(mDeviceAddress);
                        connect();
                    }
                    break;

                case TribeBleService.ACTION_CONNECTED:
                    // Successfully connected to the selected Tribe Wearables device.

                    log("Connected!");
                    mDeviceConnected = true;
                    break;

                case TribeBleService.ACTION_DISCONNECTED:
                    // The previously connected Tribe Wearables device has been disconnected.

                    mStartButton.setEnabled(false);
                    mStopButton.setEnabled(false);
                    mSendButton.setEnabled(false);
                    mUploadButton.setEnabled(false);
                    mFormatButton.setEnabled(false);
                    log("Disconnected!");

                    log("Trying to re-connect...");
                    if (mDeviceConnected) {
                        connect();
                    } else {
                        scan();
                    }
                    break;

                case TribeBleService.ACTION_READY:
                    // At this point the device is connected, its services discovered and all
                    // necessary notifications / indications are enabled. The Tribe Wearables device
                    // is ready to receive commands.

                    mStartButton.setEnabled(!mInWorkout);
                    mStopButton.setEnabled(mInWorkout);
                    mSendButton.setEnabled(!mInWorkout);
                    mUploadButton.setEnabled(!mInWorkout);
                    mFormatButton.setEnabled(!mInWorkout);
                    log("Device ready!");
                    break;

                case TribeBleService.ACTION_BATTERY_RECEIVED:
                    // This is just a battery status update. The Intent contains the battery level.

                    int battery = intent.getIntExtra("level", 0);
                    if (battery >= 0) {
                        mBattery.setText(Integer.toString(battery) + "%");
                    }
                    break;

                case TribeBleService.ACTION_DATA_RECEIVED:
                    // The device has transmitted a new set of workout-related data. The Intent
                    // contains a JSON-encoded String which can be live data during a workout or a
                    // response to a command / request to the Tribe Wearables device.

                    String data = intent.getStringExtra("data");
                    Log.d(TAG, "onReceive: data = " + data);
                    log(data);

                    // If a request for a full set of workout data to the Tribe Wearables device
                    // precedes this broadcast, this is not live workout data and should be treated
                    // accordingly.
                    if (mDataRequested) {
                        mDataRequested = false;
                        process(data);
                    }
                    break;

                case TribeBleService.ACTION_REPETITIONS_RECEIVED:
                    // During a workout the Tribe Wearables device count the repetitions (how many
                    // steps, squats etc) the user has performed. This action includes in its Intent
                    // the number of these repetitions. This count is also included in the workout
                    // data, so this action intended for UI updates.

                    short repetitions = intent.getShortExtra("repetitions", (short) 0);
                    mTextRepetitions.setText(Short.toString(repetitions));
                    break;

                case TribeBleService.ACTION_WORKOUT_LIST_RECEIVED:
                    // The Tribe Wearables device saves workouts in its built-in flash memory, in
                    // order to prevent data loss in case the Android device becomes unavailable
                    // during a workout. This action includes a JSON-encoded String with all the
                    // workout data sets currently present in the Tribe Wearables device's flash.
                    // (refer to the wiki on how to manage the built-in flash)

                    mDataRequested = true;
                    if (mServiceConnected) mService.fetchLastWorkout();
                    break;

                default:
                    break;

            }
        }
    };

    /**
     * Processes the given data set. For the purposes of this demo it saves the data in the
     * Android device's external storage for further processing and shares the saved file.
     *
     * @param data The JSON-encoded String of data received from the Tribe Wearables device.
     */
    private void process(String data) {
        // If something goes wrong with the data transmission or processing in the service, a
        // null String is included in the broadcast Intent.
        if (data == null) {
            Log.e(TAG, "No data retrieved!");
            return;
        }

        // Print the data in the app's log TextView.
        log(data);

        // Prepare the filename for the newly retrieved data.
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String device = mDeviceAddress.replaceAll(":", "");
        String filename = device + "_" + dateFormat.format(now) + ".json";

        // Check if the external storage is writable.
        if (!isExternalStorageWritable()) {
            Log.e(TAG, "External storage not writable!");
            return;
        }

        try {
            // Create the data directory.
            File directory = new File(Environment.getExternalStorageDirectory() + "/Tribe");
            if (!directory.exists() && !directory.mkdirs()) {
                log("Could not create directory!");
                return;
            }

            // Create the file to be filled.
            File file = new File(directory.getPath() + "/" + filename);
            if (!file.createNewFile()) {
                log("Could not create file!");
                return;
            }

            // Write the data to the file and close it.
            Writer output = new BufferedWriter(new FileWriter(file));
            output.write(data);
            output.close();

            // Notify the user that her data were successfully saved, including the full path.
            Toast.makeText(getApplicationContext(), "Workout data saved in: " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();

            // Share the newly created file.
            if (mSend) send(file);
            else upload(file);
        } catch (Exception e) {
            Log.e(TAG, "Failed to write file!", e);
        }
    }

    //endregion Service Communication (Reception)

    //region Unrelated Android-specific Code & Helper Methods

    /* State Preservation */
    private static final String CONNECTION_STATE = "com.tribewearables.tribewearablesdemo.MainActivity.CONNECTION_STATE";
    private static final String CONNECTED_DEVICE = "com.tribewearables.tribewearablesdemo.MainActivity.CONNECTED_DEVICE";
    private static final String LATEST_DATA = "com.tribewearables.tribewearablesdemo.MainActivity.LATEST_DATA";
    private static final String LATEST_REPETITIONS = "com.tribewearables.tribewearablesdemo.MainActivity.LATEST_REPETITIONS";
    private static final String WORKOUT_STATE = "com.tribewearables.tribewearablesdemo.MainActivity.WORKOUT_STATE";

    /* BLE Service */
    private boolean mServiceConnected;
    private TribeBleService mService;

    // Device connection
    private boolean mDeviceConnected;
    private String mDeviceAddress;

    // Workout state
    private boolean mInWorkout;

    // Workout data acquisition
    private boolean mDataRequested;
    private String mStringData;
    private JSONArray mData;

    /* Permission Requests */
    private boolean mPermissionsGranted;
    private static final int REQUEST_PERMISSIONS = 0;
    private static final int REQUEST_ENABLE_BLE = 1;

    /* GUI Elements */
    private TextView mTextLog;
    private TextView mTextRepetitions;
    private Button mStartButton;
    private Button mStopButton;
    private Button mSendButton;
    private Button mUploadButton;
    private Button mFormatButton;
    private TextView mBattery;
    private ScrollView mScrollView;

    private boolean mSend = true;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseStorage mStorage;

    /**
     * Permissions required for the app to be fully functional.
     */
    private static final String[] requiredPermissions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get handles for the GUI elements.
        mTextLog = findViewById(R.id.text_log);
        mTextRepetitions = findViewById(R.id.text_repetitions);

        mStartButton = findViewById(R.id.button_start);
        mStartButton.setOnClickListener(v -> startWorkout());

        mStopButton = findViewById(R.id.button_stop);
        mStopButton.setOnClickListener(v -> stopWorkout());

        mSendButton = findViewById(R.id.button_send);
        mSendButton.setOnClickListener(v -> {
            mSend = true;
            listWorkouts();
        });

        mUploadButton = findViewById(R.id.button_upload);
        mUploadButton.setOnClickListener(v -> {
            mSend = false;
            listWorkouts();
        });

        mFormatButton = findViewById(R.id.button_format);
        mFormatButton.setOnClickListener(v -> {
            log("Formatting memory... Please wait until the red light stops blinking!");
            formatMemory();
        });

        mBattery = findViewById(R.id.text_battery);
        mScrollView = findViewById(R.id.scroll_log);

        // Initialize the connection status properties.
        mPermissionsGranted = false;
        mServiceConnected = false;
        mInWorkout = false;

        // This section is required for Android 6.0 (Marshmallow).
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> ungranted = new ArrayList<>();
            for (String permission : requiredPermissions) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                    ungranted.add(permission);
            }

            if (ungranted.size() > 0) {
                requestPermissions(ungranted.toArray(new String[0]), REQUEST_PERMISSIONS);
            } else {
                mPermissionsGranted = true;
            }
        } else {
            mPermissionsGranted = true;
        }

        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInAnonymously:complete");
                            FirebaseUser user = mAuth.getCurrentUser();
                            mStorage = FirebaseStorage.getInstance();
                        } else {
                            Log.w(TAG, "signInAnonymously:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        // Check if we got all the required permissions.
        mPermissionsGranted = true;
        for (int result : grantResults) {
            mPermissionsGranted = mPermissionsGranted && result == PackageManager.PERMISSION_GRANTED;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPermissionsGranted)
            startBluetooth();

        final IntentFilter filter = new IntentFilter();
        filter.addAction(TribeBleService.ACTION_BLESCAN_RESULT);
        filter.addAction(TribeBleService.ACTION_CONNECTED);
        filter.addAction(TribeBleService.ACTION_DISCONNECTED);
        filter.addAction(TribeBleService.ACTION_READY);
        filter.addAction(TribeBleService.ACTION_BATTERY_RECEIVED);
        filter.addAction(TribeBleService.ACTION_REPETITIONS_RECEIVED);
        filter.addAction(TribeBleService.ACTION_WORKOUT_LIST_RECEIVED);
        filter.addAction(TribeBleService.ACTION_DATA_RECEIVED);
        registerReceiver(mBleUpdateReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mServiceConnected) {
            mService.disconnect();
            unbindService(mServiceConnection);
        }

        unregisterReceiver(mBleUpdateReceiver);
        mService = null;
        mServiceConnected = false;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            mDeviceConnected = savedInstanceState.getBoolean(CONNECTION_STATE);
            mDeviceAddress = savedInstanceState.getString(CONNECTED_DEVICE);
            mTextLog.setText(savedInstanceState.getString(LATEST_DATA));
            mInWorkout = savedInstanceState.getBoolean(WORKOUT_STATE);

            mStartButton.setEnabled(!mInWorkout);
            mStopButton.setEnabled(mInWorkout);
            mSendButton.setEnabled(!mInWorkout);
            mUploadButton.setEnabled(!mInWorkout);
            mFormatButton.setEnabled(!mInWorkout);

            String reps = savedInstanceState.getString(LATEST_REPETITIONS);
            if (reps != null) mTextRepetitions.setText(reps);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(CONNECTION_STATE, mDeviceConnected);
        outState.putString(CONNECTED_DEVICE, mDeviceAddress);
        outState.putBoolean(WORKOUT_STATE, mInWorkout);
        outState.putString(LATEST_DATA, mTextLog.getText().toString());

        if (!mTextRepetitions.getText().toString().equals("0"))
            outState.putString(LATEST_REPETITIONS, mTextRepetitions.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_ENABLE_BLE) {
            Log.d(TAG, "onActivityResult: resultCode" + resultCode);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Logs a message in the log TextView and in Logcat.
     * @param msg The message to be logged.
     */
    private void log(String msg) {
        Log.d(TAG, msg);
        mTextLog.append(msg);
        mTextLog.append("\n");
        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
    }

    /**
     * Shares the given file.
     * @param file The file to be shared.
     */
    private void send(File file) {
        mSendButton.setEnabled(false);
        mUploadButton.setEnabled(false);
        Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Share / Save Results"));
    }

    private void upload(File file) {
        mSendButton.setEnabled(false);
        mUploadButton.setEnabled(false);
        Uri uri = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
        StorageReference runsref = mStorage.getReference().child("demo_workouts/" + uri.getLastPathSegment());
        UploadTask task = runsref.putFile(uri);

        // Register observers to listen for when the upload is done or if it fails.
        task.addOnFailureListener(e -> {
            log("Failed to upload file.");
            Toast.makeText(MainActivity.this, "Could not upload file.", Toast.LENGTH_SHORT).show();
        }).addOnSuccessListener(taskSnapshot -> {
            log("File uploaded successfully.");
            Toast.makeText(MainActivity.this, "File uploaded!", Toast.LENGTH_SHORT).show();
        });
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    //endregion Unrelated Android-specific Code & Helper Methods
}
